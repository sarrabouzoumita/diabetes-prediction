This project aims to help doctors or even simple patients to predict if a person has diabetes or not.
The dataset used is taken from kaggle : https://www.kaggle.com/uciml/pima-indians-diabetes-database 
This project uses Logistic Regression to train the model then test it's accuracy using spark's libraries and its implemented methods.

The features used for training are : - Glucose level
- Pregnancies
- Blood pressure
- Skin thickness
- Insulin
- BMI
- Diabetes Pedigree Function 
- Age
